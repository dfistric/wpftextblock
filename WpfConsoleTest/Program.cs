﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WpfClassLibrary;

namespace WpfConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {

            // OpenWpfWindowDirectFromConsoleApp();
            OpenWpfWindowOverClassLibraryFunction();
        }

        /// <summary>
        /// Opens a MainWindow in WpfClassLibrary directly from Console App
        /// </summary>

        private static void OpenWpfWindowDirectFromConsoleApp()
        {
            Thread viewThread = new Thread(() =>
            {
                MainWindow view = new MainWindow();
                view.ShowDialog();
            });

            viewThread.SetApartmentState(ApartmentState.STA);
            viewThread.Start();

        }
        /// <summary>
        /// Opens a MainWindow in WpfClassLibrary by calling WPFClassLibrary function
        /// Simulating a call from COM dll
        /// It will work from here but if it's called from Excel it will crash if WpfClassLibrary is referencing MaterialDesignThemes 2.6.0
        /// 
        /// 
        /// </summary>
        private static void OpenWpfWindowOverClassLibraryFunction()
        {
            StartMe sm = new StartMe();
            sm.OpenWpfWindowFromFunctionInClassLibrary();
        }


    }
}
