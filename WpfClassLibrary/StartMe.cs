﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WpfClassLibrary
{
    public class StartMe
    {
        public void OpenWpfWindowFromFunctionInClassLibrary()
        {
            Thread viewThread = new Thread(() =>
            {
                // If the WpfClassLibrary is referencing  MaterialDesignThemes 2.2.1.750  it will work from COM like Excel
                // but it will crash here with an error:
                // System.IO.FileNotFoundException: 'Could not load file or assembly 'MaterialDesignColors, Version = 1.2.0.325, Culture = neutral, PublicKeyToken = null' or one of its dependencies.
                // The system cannot find the file specified.'

                // In this project you can find WpfClassTest.xlsm  which you can open in Excel when Debug launches it as external program and click CommandButton1 or call
                // in ImmediatePane function TestWPFClassLibrary() -- see Excel.PNG 

                MainWindow view = new MainWindow();
                view.ShowDialog();
            });

            viewThread.SetApartmentState(ApartmentState.STA);
            viewThread.Start();
        }


    }
}
