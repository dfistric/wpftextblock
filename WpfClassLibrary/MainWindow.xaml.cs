﻿using MaterialDesignColors;
using MaterialDesignThemes.Wpf;
using System.Windows;
using System.Windows.Controls;

namespace WpfClassLibrary
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {

            ColorZoneAssist.SetMode(new GroupBox(), ColorZoneMode.Accent);
            Hue hue = new Hue("name", System.Windows.Media.Color.FromArgb(1, 2, 3, 4), System.Windows.Media.Color.FromArgb(1, 5, 6, 7));

            // As Marija mentioned if you don't put this two dummy lines, one for the MaterialDesignColors and one for the MaterialDesignThemes.Wpf
            // code breaks on InitializeComponent() with an error:
            // System.Windows.Markup.XamlParseException: ''Set property 'System.Windows.ResourceDictionary.Source' threw an exception.' Line number '12' and line position '18'.'
            // Inner Exception
            // FileNotFoundException: Could not load file or assembly 'MaterialDesignThemes.Wpf, Culture=neutral' or one of its dependencies.The system cannot find the file specified.
            
            
            InitializeComponent();

            // When you call this MainWindow from WpfConsoleTest it works while we have latest MaterialDesignThemes installed, in my case 2.6.0 Stable 
            // but it doesn't work if you have MaterialDesignThemes 2.2.1.750 which is needed to work from COM in Excel, Access ....


            // In this project you can find WpfClassTest.xlsm  which you can open in Excel when Debug launches it as external program and click CommandButton1 or call
            // in ImmediatePane function TestWPFClassLibrary() -- see Excel.PNG 
            // When we call the function OpenWpfWindowFromFunctionInClassLibrary() from Excel while we have referenced MaterialDesignThemes 2.2.1.750 everything works
            // but if MaterialDesignThemes 2.6.0 is referenced it gives following error:

            // System.Windows.Markup.XamlParseException: 'Provide value on 'System.Windows.Markup.StaticResourceHolder' threw an exception.'
            // Inner Exception
            // NotImplementedException: The method or operation is not implemented.

        }
    }
}
